## Konvertor pro převod souborů z NÚV

Postup zpracování:
1. Načtení NÚV souboru (*.ISO)
2. Převedení do seznamu
3. Načtení seznamu
4. Zpracování seznamu

Surová data z NÚV byla poslána poškozená, proto je nutný krok 2, ve kterém se zpracují surová data.
Ze surových dat se exportují pouze rozpoznatelné (a nutné) hodnoty. Všechny důležité operace 
provádí třída NUV. 

### Příklad použití
Skript zpracovává vždy pouze jeden soubor - 
``` bash
python nuv_parser.py ./source/NUV_2016.ISO ./temp/out.xml
```

Pro zpracování více souborů lze použít bash skript mass_convert.sh, který zpracuje soubor _data_.
V něm jsou vždy na každém řádku _zdroj_ _cíl_ 

Lze tedy zdrojový a cílový soubor mít kdekoli, kam skript _dosáhne_.
### Jména
Po zkonvertování doporučuji zkontrolot jména. Ve zdroji jsou jména autorů místy nejednotná
(ve smyslu struktury a obsahu), skript by měl nicméně odstranit většinu chyb.

### Defaultní hodnoty
Lze přidat defaultní hodnoty k polím. To neznamená natvrdo, pouze to dosadí defaultní hodnotu
**pouze** v případě, že nebyla ve zdroji (kupříkladu pole 041, v některých záznamech chybí nebo je vadný, 
takže se dosadí 'cze'). Není to příliš elegantní, ale některá pole **jsou** nutná a toto je způsob jak je odchytat.