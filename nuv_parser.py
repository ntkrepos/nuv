# coding: utf-8

import argparse

from classes import NUV

# Arguments
# Example: python nuv_parser.py ./source/NUV_2016.ISO ./temp/out.xml
parser = argparse.ArgumentParser()
parser.add_argument("source", help="source file", type=str)
parser.add_argument("target", help="target file", type=str)
args = parser.parse_args()


if __name__ == "__main__":
    with open(args.source, encoding='cp1250') as fh:
        parser = NUV(fh.read())
        parser.make_collection()

        # Write to file
        with open(args.target, 'wb+') as doc:
            doc.write(parser.__bytes__())
