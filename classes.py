# coding: utf-8

from lxml import etree

# Simple field ('035', '001  ', 'a', ' ', ' ', 'a')
# (target_tag, source_key, source_dict_key, target_ind1, target_ind2, target_subfield_code)
simple_fields_data = [

    ('598', '1300 ', 'a', ' ', ' ', 'a'),
    ('246', '???10', 'b', '3', '3', 'a'),
    ('245', '???10', 'a', '0', '0', 'a'),
    ('041', '???0 ', 'a', '0', '7', 'a'),
    ('300', '500  ', 'a', ' ', ' ', 'a'),
    ('035', '001  ', ' ', ' ', ' ', 'a'),
    ('046', '??? 1', 'c', ' ', ' ', 'k')

]

# Static data for NUV
static_data_list = [

    ('340', ' ', ' ', 'a', 'text/pdf'),
    ('980', ' ', ' ', 'a', 'metodiky'),
    ('856', '0', ' ', 'f', 'natasa.kevic@nuv.cz'),
    ('998', ' ', ' ', 'a', 'nuv')

]

# Separators for raw parser
SEP_RECS = '\x1d'
SEP_FIELDS = '\x1e'
SEP_SUBS = '\x1f'


class NUV:
    """
Engine for parser. It contains the main methods.
From raw marc file(*.iso, *.dat, ...) -> tuple -> *.xml
    """

    def __init__(self, raw_text: str):
        self.record = None
        self.static_data = static_data_list
        self.actual_row = None
        self.source = None
        self.root = None
        self.raw_text = raw_text

    def __str__(self) -> str:
        """
        For simple output as string
        """
        return etree.tostring(self.root, pretty_print=True,
                              xml_declaration=True, encoding="utf-8")

    def __bytes__(self) -> bytes:
        """
        For write to file
        """
        return etree.tostring(self.root, pretty_print=True,
                              xml_declaration=True, encoding="utf-8")

    def create_field(self, tag: str, value: str, ind1: str = " ",
                     ind2: str = " ", code: str = "a"):
        """
        Create xml element
        <datafield>
            <subfield></subfield>
        </datafield>
        """
        if value:
            nusl_field = etree.SubElement(
                self.record, 'datafield', tag=tag, ind1=ind1, ind2=ind2)

            nusl_field = etree.SubElement(nusl_field, 'subfield', code=code)
            nusl_field.text = value

    def add_static_field(self):
        for field in self.static_data:
            datafield = etree.SubElement(
                self.record,
                'datafield',
                tag=field[0],
                ind1=field[1],
                ind2=field[2])

            subfield = etree.SubElement(datafield, 'subfield', code=field[3])
            subfield.text = field[4]

    def cleanup(self):
        """
        Remove empty xml elements.
        Example:
        <datafield>
            <subfield></subfield>
        </datafield>
        :return:
        """
        for datafield in self.record.findall('datafield'):
            subfield = datafield.find('subfield')
            if subfield is None or subfield.text == "":
                self.record.remove(datafield)

        if len(self.record) < 4:
            self.root.remove(self.record)

    @staticmethod
    def get_key(elem: etree.ElementBase) -> str:

        return str(elem.get('tag'))

    def sort_by_tag(self):
        """
        Sort datafields by tag
        """
        container = self.record

        container[:] = sorted(container, key=self.get_key)
        self.record = container

    def get_value(self, source_id: str, source_sub_code: str = 'a'):
        """
        Get value from tuple.
        """
        tuple_id, val = self.actual_row

        if isinstance(val, str):
            return val if tuple_id.strip() == source_id.strip() else False

        if isinstance(val, dict):
            try:
                return val[source_sub_code] if tuple_id == source_id else False
            except KeyError:
                return False
        return False

    def row_parser(self, row: tuple):
        self.actual_row = row
        for data in simple_fields_data:
            tag, source_key, value, ind1, ind2, code = data
            source_value = self.get_value(source_key, value)
            self.create_field(tag, source_value,
                              ind1=ind1,
                              ind2=ind2,
                              code=code)

        # Get name
        name = self.get_value('???10', 'c')

        if name:
            for name in self.get_names(name):
                self.create_field('720', name, ind1=' ', ind2=' ', code='a')

        # 260
        # ('260', ' ', ' ', 'b', 'NÚV')
        self.field_260_b()

        # Filter value in 020 field
        field_020 = self.get_value('???0 ', source_sub_code='z')

        if field_020:

            if '-80-' in field_020:
                self.create_field(
                    '020', field_020, ind1=' ', ind2=' ', code='a')

        # 856 field
        self.field_856()

        # Summary field
        summary = self.get_value('???00', source_sub_code='t')

        if summary:
            nusl_520__a_orig = etree.SubElement(
                self.record, 'datafield', tag="520", ind1=" ", ind2=" ")

            # Subfield code a
            nusl_520__a = etree.SubElement(nusl_520__a_orig, 'subfield', code='a')

            nusl_520__a.text = summary
            # Subfield code 9
            nusl_520__9 = etree.SubElement(nusl_520__a_orig, 'subfield', code='9')

            nusl_520__9.text = 'cze'

        # Special FFT field
        link = self.get_value('8564 ', 'u')

        if link:
            fft_field = etree.SubElement(self.record, 'datafield', tag="FFT")
            fft_subfield = etree.SubElement(fft_field, 'subfield', code='a')
            fft_subfield.text = link
            fft_subfield_text = etree.SubElement(fft_field, 'subfield', code='z')
            fft_subfield_text.text = 'Plný text'

    def field_856(self):
        field_856 = self.get_value('8564 ', source_sub_code='u')
        if field_856:
            nusl_856 = etree.SubElement(
                self.record, 'datafield', tag='856', ind1='4', ind2='2')

            # Subfield code z
            nusl_856_sub_z = etree.SubElement(nusl_856, 'subfield', code='z')
            nusl_856_sub_z.text = 'Elektronické umístění souboru'

            # Subfield
            nusl_856_sub_u = etree.SubElement(nusl_856, 'subfield', code='u')
            nusl_856_sub_u.text = field_856

    def field_260_b(self):
        field_260_b = self.get_value('??? 1', source_sub_code='b')
        if field_260_b:
            nusl_260 = etree.SubElement(
                self.record, 'datafield', ind1=' ', ind2=' ', tag='260')
            nusl_260_sub_b = etree.SubElement(nusl_260, 'subfield', code='b')
            if field_260_b == "NÚV":
                field_260_b = "Národní ústav pro vzdělávání"
            nusl_260_sub_b.text = field_260_b

    def add_default_field(self):
        """
        Add default data if field not exist
        """
        # Check 041
        doc_lang = self.record.find(".//datafield[@tag='041']")
        if doc_lang is None:
            self.create_field('041', 'cze',
                              ind1='0',
                              ind2='7',
                              code='a')

    def make_collection(self):
        self.parse_raw_nuv()
        self.root = etree.Element('collection')
        for rec in self.source:

            self.record = etree.SubElement(self.root, 'record')
            for row in rec:
                self.row_parser(row)

            self.add_static_field()
            self.add_default_field()
            self.sort_by_tag()
            self.cleanup()

    def parse_raw_nuv(self):
        """
        Convert raw nuv file(marc) to tuple
        """

        recs = self.raw_text.split(SEP_RECS)
        parsed_recs = []

        for rec in recs:

            leader_section, *fields = rec.split(SEP_FIELDS)

            field_list_section = leader_section[24:]
            field_list = [field_list_section[i:i + 3] for i in range(0, len(field_list_section), 12)]

            parsed_fields, parsed_rec = [], []
            for field in fields:

                indicators, *subs = field.split(SEP_SUBS)

                if indicators:
                    subs_dict = {s[0]: s[1:] for s in subs}
                    parsed_fields.append((indicators, subs_dict))

            for (field_code, (ind, subs_dict)) in zip(field_list, parsed_fields):

                if len(ind) == 2:
                    parsed_rec.append((field_code + ind, subs_dict))
                else:
                    parsed_rec.append((field_code, ind))

            parsed_recs.append(sorted(parsed_rec, key=lambda x: x[0]))

        self.source = parsed_recs

    @staticmethod
    def build_name(striped_name: list) -> str:

        length = len(striped_name)

        if length == 1:

            return '%s' % striped_name[0]

        elif length == 2:
            if striped_name[0] == "Národní" or striped_name[1] == 'Národní':
                return ""

            return '%s, %s' % (striped_name[1], striped_name[0])

        else:
            if striped_name[2] == "Národní" or striped_name[1] == 'Národní':
                return ""

            return '%s, %s' % (striped_name[2], striped_name[1])

    def get_names(self, unconverted_names: str) -> list:

        name = unconverted_names
        result_name = []

        if not name.startswith('NÚV'):

            names = name.split(',')

            for name in names:
                if name == 'kolektiv autorů':
                    continue

                striped_name = name.split(' ')

                result_name.append(self.build_name(striped_name))

        return result_name
