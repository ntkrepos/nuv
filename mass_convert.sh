#!/usr/bin/env bash

BAD_SOURCE=()
while read source target;
do
    python3 nuv_parser.py ${source} ${target}
    exit_status=$?
    if [ "${exit_status}" -ne 0 ];
    then
        BAD_SOURCE=("$BAD_SOURCE[@]" ${source})
    fi
done < "data"

if [ ${#BAD_SOURCE[@]} -ne 0 ];
then
    for source in "${BAD_SOURCE[-1]}"
    do
        echo "File processing error - "${source}
    done
else
    echo 'All right!'
fi